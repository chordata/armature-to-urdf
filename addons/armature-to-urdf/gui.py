from bpy.types import Panel
from bl_ui.properties_data_bone import BoneButtonsPanel

urdf_id_name = 'export_armature.urdf'

class DATA_PT_urdf_props(BoneButtonsPanel, Panel):
	bl_label = "URDF export"
	bl_order = 2

	def draw(self, context):
		layout = self.layout

		ob = context.object
		arm = context.armature
		# space = context.space_data

		bone = context.bone
		if not bone:
			row = layout.row()
			row.label(text="", icon='BONE_DATA')
			row.prop(context.edit_bone, "body_segment_mass", text="Mass")
			row = layout.row()
			row.label(text="Inertia Matrix", icon='BONE_DATA')
			row = layout.row()
			row.prop(context.edit_bone, "ixx", text="XX")
			row.prop(context.edit_bone, "iyy", text="YY")
			row.prop(context.edit_bone, "izz", text="ZZ")
			row = layout.row()
			row.prop(context.edit_bone, "ixy", text="XY")
			row.prop(context.edit_bone, "ixz", text="XZ")
			row.prop(context.edit_bone, "iyz", text="YZ")


		if bone:
			row = layout.row()
			row.label(text="", icon='BONE_DATA')
			row.prop(bone, "body_segment_mass", text="Mass")

			row = layout.row()
			row.label(text="Inertia Matrix", icon='BONE_DATA')
			row = layout.row()
			row.prop(bone, "ixx", text="XX")
			row.prop(bone, "iyy", text="YY")
			row.prop(bone, "izz", text="ZZ")
			row = layout.row()
			row.prop(bone, "ixy", text="XY")
			row.prop(bone, "ixz", text="XZ")
			row.prop(bone, "iyz", text="YZ")

		layout.separator()
		layout.operator(urdf_id_name, text="Export URDF")


def register():
	from bpy.utils import register_class
	register_class(DATA_PT_urdf_props)


def unregister():
	from bpy.utils import unregister_class
	unregister_class(DATA_PT_urdf_props)
