import bpy
import bgl
import blf
import gpu
from gpu_extras.batch import batch_for_shader
from math import pi, cos, sin
from mathutils import Vector


def draw_callback_px(context):

    # the_path = [(0,0,0), (100,0,0), (100,100,0), (0,100,0)]
    # lamp_loc = context.scene.objects['Light'].location
    # camera_loc = context.scene.objects['Camera'].location
    # cube_loc = context.scene.objects['Cube'].location
    # the_path = [lamp_loc, camera_loc, cube_loc, lamp_loc]
    
    ob = context.object
    data = ob.data
    r = 0.5
    steps = 32

    bone = data.bones.active 
    M_bone = None
    if not bone:
        bone = data.edit_bone.active
        M_bone = context.object.matrix_world @ bone.matrix
    else:
        M_bone = context.object.matrix_world @ bone.matrix_local

    # x= r * Cos[Phi] * Sin[Theta]
    # y= r * Sin[Phi] * Sin[Theta]
    # z= r * Cos[Theta]

    the_path = []#(start, end )
    for s in range(steps):
        theta = (s*pi*2)/steps
        loc = Vector()
        loc.x += r * sin(theta) 
        loc.z += r * cos(theta)
        loc = M_bone @ loc
        the_path.append(loc)

    # 50% alpha, 2 pixel width line
    bgl.glEnable(bgl.GL_BLEND)
    bgl.glEnable(bgl.GL_LINE_SMOOTH)
    bgl.glDisable(bgl.GL_DEPTH_TEST)

    shader = gpu.shader.from_builtin('3D_UNIFORM_COLOR')
    bgl.glLineWidth(2)
    batch = batch_for_shader(shader, 'LINE_LOOP', {"pos": the_path})
    # print("draw_update", the_path)
    shader.bind()
    shader.uniform_float("color", (0.0, 1.0, 0.0, 1.0))
    batch.draw(shader)

    # restore opengl defaults
    bgl.glLineWidth(1)
    bgl.glDisable(bgl.GL_BLEND)
    bgl.glDisable(bgl.GL_LINE_SMOOTH)
    bgl.glEnable(bgl.GL_DEPTH_TEST)

class ModalDrawOperator(bpy.types.Operator):
    bl_idname = "view3d.urdf_view_props"
    bl_label = "Display URDF armature properties"

    @classmethod
    def poll(cls, context):
        ob = context.object
        data = ob.data
        return ob and data and (data.bones.active or data.edit_bone.active)

    def modal(self, context, event):
        context.area.tag_redraw()

        # if event.type == 'MOUSEMOVE':
        #     self.mouse_path.append((event.mouse_region_x, event.mouse_region_y))

        # elif event.type == 'LEFTMOUSE':
            # bpy.types.SpaceView3D.draw_handler_remove(self._handle, 'WINDOW')
        #     return {'FINISHED'}

        if event.type  == 'ESC':
            bpy.types.SpaceView3D.draw_handler_remove(self._handle, 'WINDOW')
            return {'CANCELLED'}


        return {'RUNNING_MODAL'}

    def invoke(self, context, event):
        if context.area.type == 'VIEW_3D':

            # Add the region OpenGL drawing callback
            # draw in view space with 'POST_VIEW' and 'PRE_VIEW'
            self._handle = bpy.types.SpaceView3D.draw_handler_add(draw_callback_px, (context,), 'WINDOW', 'POST_VIEW')

            self.mouse_path = []

            context.window_manager.modal_handler_add(self)
            return {'RUNNING_MODAL'}
        else:
            self.report({'WARNING'}, "View3D not found, cannot run operator")
            return {'CANCELLED'}


#     def invoke(self, context, event):
#         area = context.area

#         if area.type != 'VIEW_3D':
#             areas = get_view_3D_areas(context)

#             if not areas:
#                 self.report({'WARNING'}, "View3D not found, cannot run operator")
#                 return {'CANCELLED'}

#             area = areas[0]
#             context.area = area

#             # Add the region OpenGL drawing callback
#             # draw in view space with 'POST_VIEW' and 'PRE_VIEW'
#             self._handle = bpy.types.SpaceView3D.draw_handler_add(draw_callback_px, (context,), 'WINDOW', 'POST_VIEW')

#             context.window_manager.modal_handler_add(self)
#             return {'RUNNING_MODAL'}


# def get_view_3D_areas(context):
#     areas = []
#     for area in context.screen.areas:
#         if area.type == 'VIEW_3D':
#             areas.append(area)

#     return areas