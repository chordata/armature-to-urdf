import bpy
import xml.etree.ElementTree as ET
import xml.dom.minidom  as minidom
import math 
from mathutils import Matrix, Vector
from random import choice

# Colors from https://colorbrewer2.org/
colors = [(166,206,227), (31,120,180), (178,223,138), (51,160,44), (251,154,153), (227,26,28), (253,191,111), (255,127,0), (202,178,214), (106,61,154), (255,255,153), (177,89,40)]
colors = [(c[0]/255, c[1]/255, c[2]/255) for c in colors]

# state vars
ob = None
counter = 0
links = []
joints = []

# constants
LINK_NAME_FORMAT = "link_{counter:02d}_{bone_name}"
JOINT_NAME_FORMAT = "joint_{counter:02d}_{bone_name}"
JOINT_TYPE = "revolute"
ORIGIN_NODE_FLOAT_PRECISION = 6
ORIGIN_NODE_FORMAT = "{{:.{0}f}} {{:.{0}f}} {{:.{0}f}}".format(ORIGIN_NODE_FLOAT_PRECISION)
ZERO_ORIGIN_NODE = ET.fromstring('<origin xyz="0 0 0" rpy="0 0 0"/>')
INERTIA_NODE_FMT = '<inertia ixx="{}" ixy="{}" ixz="{}" iyy="{}" iyz="{}" izz="{}" />'
BASE_LIMIT_NODE_STR = None

def set_base_limit_str(effort, velocity):
    global BASE_LIMIT_NODE_STR
    BASE_LIMIT_NODE_STR = '<limit effort="{:.4f}" lower="-1.57079632679" upper="1.57079632679" velocity="{:.4f}"/>'.format(
        effort, velocity)

set_base_limit_str(100, 3)



class Armature_Structure_Error(Exception):
    pass

# define a function to recursively walk the complete armature hierarchy
def walk_armature(this_bone, handler):
    handler(this_bone)
    for child in this_bone.children:
        walk_armature(child, handler)

def create_base_link():
    this_xml_link = ET.Element('link')
    this_xml_link.set("name", "base_link")
    links.append(this_xml_link)


def bone_to_urdf(this_bone):
    """ This function extracts the basic properties of the bone and populates
    links and joint lists with the corresponding urdf nodes"""
    
    global counter
    this_bone.xml_link_name = LINK_NAME_FORMAT.format(counter = counter, bone_name = this_bone.name)
    
    # Create the joint xml node
    if this_bone.parent:
        this_xml_link = create_bone_link(this_bone)
    else:
        this_xml_link = create_root_bone_link(this_bone)


    this_xml_link.append(ET.fromstring(INERTIA_NODE_FMT.format(this_bone.ixx, this_bone.ixy, this_bone.ixz,
                                    this_bone.iyy, this_bone.iyz, this_bone.izz)))
    this_xml_link.append(ET.fromstring('<mass value="{:.6f}"/>'.format(this_bone.body_segment_mass)))

    # Create the visual node
    this_color = choice(colors)
    this_xml_geom = ET.Element('geometry')
    this_xml_box = ET.Element('box')
    padding = min(this_bone.length/25, 0.02)
    this_xml_box.set("size", "0.02 {} 0.02".format(this_bone.length - padding*2))
    this_xml_geom.append(this_xml_box)

    this_xml_material = ET.Element('material')
    this_xml_material.set("name", "mat_col_{}".format(this_color))
    this_xml_color = ET.Element('color')
    this_xml_color.set("rgba", "{:.2f} {:.2f} {:.2f} 1.0".format(*this_color))
    this_xml_material.append(this_xml_color)

    this_xml_visual = ET.Element('visual')
    this_xml_visual.append(ET.fromstring('<origin rpy="0 0 0" xyz="0 {} 0"/>'.format(this_bone.length/2 + padding)))  
    this_xml_visual.append(this_xml_geom)
    this_xml_visual.append(this_xml_material)

    this_xml_link.append(this_xml_visual)


    counter += 1

def create_root_bone_link(this_bone):
    this_matrix = this_bone.matrix_local

    base_joint_name = JOINT_NAME_FORMAT.format(counter = counter, bone_name = this_bone.name)
    r_joint_name = base_joint_name + '_R'
    p_joint_name = base_joint_name + '_P'
    y_joint_name = base_joint_name + '_Y'
    
    # full motion limit node
    limit_node = ET.fromstring(BASE_LIMIT_NODE_STR)
    limit_node.set('lower', str(-math.pi))
    limit_node.set('upper', str(math.pi))

    # ========== Create R joint ===============
    r_joint = ET.Element('joint')
    r_joint.set("name",r_joint_name)
    r_joint.set("type", JOINT_TYPE)

    parent_xml_node = ET.Element('parent')
    parent_xml_node.set("link", "base_link")
    
    r_xml_link = ET.Element('link')
    r_xml_link_name = this_bone.xml_link_name + '_R'
    r_xml_link.set("name", r_xml_link_name)
    links.append(r_xml_link)

    child_xml_node = ET.Element('child')
    child_xml_node.set("link", r_xml_link_name)
    
    r_joint.append(parent_xml_node)
    r_joint.append(child_xml_node)
    r_joint.append(ET.fromstring('<axis xyz="1 0 0"/>'))

    r_joint.append(limit_node)

    origin_xml_node = get_origin_from_matrix(this_matrix)
    
    r_joint.append(origin_xml_node)
    joints.append(r_joint)

    parent_link_name = r_xml_link_name

     # ========== Create P joint ===============
    p_joint = ET.Element('joint')
    p_joint.set("name",p_joint_name)
    p_joint.set("type", JOINT_TYPE)

    parent_xml_node = ET.Element('parent')
    parent_xml_node.set("link", parent_link_name)

    p_xml_link = ET.Element('link')
    p_xml_link_name = this_bone.xml_link_name + '_P'
    p_xml_link.set("name", p_xml_link_name)
    links.append(p_xml_link)

    child_xml_node = ET.Element('child')
    child_xml_node.set("link", p_xml_link_name)

    p_joint.append(parent_xml_node)
    p_joint.append(child_xml_node)
    p_joint.append(ET.fromstring('<axis xyz="0 1 0"/>'))

    p_joint.append(limit_node)
    p_joint.append(ZERO_ORIGIN_NODE)

    joints.append(p_joint)

    parent_link_name = p_xml_link_name  

     # ========== Create Y joint ===============
    y_joint = ET.Element('joint')
    y_joint.set("name",y_joint_name)
    y_joint.set("type", JOINT_TYPE)

    parent_xml_node = ET.Element('parent')
    parent_xml_node.set("link", parent_link_name)

    y_xml_link = ET.Element('link')
    y_xml_link_name = this_bone.xml_link_name + '_Y'
    y_xml_link.set("name", y_xml_link_name)
    links.append(y_xml_link)

    child_xml_node = ET.Element('child')
    child_xml_node.set("link", y_xml_link_name)

    y_joint.append(parent_xml_node)
    y_joint.append(child_xml_node)
    y_joint.append(ET.fromstring('<axis xyz="0 0 1"/>'))

    y_joint.append(limit_node)
    y_joint.append(ZERO_ORIGIN_NODE)

    joints.append(y_joint)

    this_bone.xml_link_name = y_xml_link_name
    return y_xml_link


def get_axial_constrain(bone):
    pose_bone = ob.pose.bones[bone.name] 
    wide_limits = (- math.pi/2, math.pi/2) # 180 deg freedom

    try:
        ctr = pose_bone.constraints['Limit Rotation']
    except KeyError as e:
        return { k : wide_limits for k in ('r', 'p', 'y') }

    limits = {}
    if ctr.use_limit_x:
        if (ctr.min_x, ctr.max_x) == (0,0):
            limits['r'] = False
        else:
            limits['r'] = (ctr.min_x, ctr.max_x)
    else:
        limits['r'] = wide_limits

    if ctr.use_limit_y:
        if (ctr.min_y, ctr.max_y) == (0,0):
            limits['p'] = False
        else:
            limits['p'] = (ctr.min_y, ctr.max_y)
    else:
        limits['p'] = wide_limits

    if ctr.use_limit_z:
        if (ctr.min_z, ctr.max_z) == (0,0):
            limits['y'] = False
        else:
            limits['y'] = (ctr.min_z, ctr.max_z)
    else:
        limits['y'] = wide_limits

    return limits

def get_origin_from_matrix(M):
    translation = M.to_translation()
    euler = M.to_euler()

    origin_xml_node = ET.Element('origin')
    origin_xml_node.set("rpy", ORIGIN_NODE_FORMAT.format( euler.x, euler.y, euler.z))
    origin_xml_node.set("xyz", ORIGIN_NODE_FORMAT.format( translation.x, translation.y, translation.z))

    return origin_xml_node



def create_bone_link(this_bone):
    global counter

    limits = get_axial_constrain(this_bone)
    # Get bone properties 
    parent_bone = this_bone.parent
    
    this_matrix = this_bone.matrix_local
    parent_matrix = parent_bone.matrix_local
    this_relative_matrix = parent_matrix.inverted() @ this_matrix 

    base_joint_name = JOINT_NAME_FORMAT.format(counter = counter, bone_name = this_bone.name) 
    r_joint_name = base_joint_name + '_R'
    p_joint_name = base_joint_name + '_P'
    y_joint_name = base_joint_name + '_Y'

    parent_link_name = parent_bone.xml_link_name


    # ------------ Create R joint-------------

    if limits['r']:
        r_joint = ET.Element('joint')
        r_joint.set("name",r_joint_name)
        r_joint.set("type", JOINT_TYPE)

        # create parent and child nodes
        parent_xml_node = ET.Element('parent')
        parent_xml_node.set("link", parent_bone.xml_link_name)

        r_xml_link = ET.Element('link')
        r_xml_link_name = this_bone.xml_link_name + '_R'
        r_xml_link.set("name", r_xml_link_name)
        links.append(r_xml_link)

        child_xml_node = ET.Element('child')
        child_xml_node.set("link", r_xml_link_name)

        r_joint.append(parent_xml_node)
        r_joint.append(child_xml_node)
        r_joint.append(ET.fromstring('<axis xyz="1 0 0"/>'))

        limit_node = ET.fromstring(BASE_LIMIT_NODE_STR)
        limit_node.set('lower', str(limits['r'][0]))
        limit_node.set('upper', str(limits['r'][1]))
        r_joint.append(limit_node)
        
        origin_xml_node = get_origin_from_matrix(this_relative_matrix)
        
        r_joint.append(origin_xml_node)
        joints.append(r_joint)

        parent_link_name = r_xml_link_name
        ret_link = r_xml_link

    # ------------ Create P joint-------------

    if limits['p']:
        p_joint = ET.Element('joint')
        p_joint.set("name",p_joint_name)
        p_joint.set("type", JOINT_TYPE)

        # create parent and child nodes
        parent_xml_node = ET.Element('parent')
        parent_xml_node.set("link", parent_link_name)

        p_xml_link = ET.Element('link')
        p_xml_link_name = this_bone.xml_link_name + '_P'
        p_xml_link.set("name", p_xml_link_name)
        links.append(p_xml_link)

        child_xml_node = ET.Element('child')
        child_xml_node.set("link", p_xml_link_name)
        
        p_joint.append(parent_xml_node)
        p_joint.append(child_xml_node)
        p_joint.append(ET.fromstring('<axis xyz="0 1 0"/>')) 
        
        limit_node = ET.fromstring(BASE_LIMIT_NODE_STR)
        limit_node.set('lower', str(limits['p'][0]))
        limit_node.set('upper', str(limits['p'][1]))
        p_joint.append(limit_node)

        if parent_link_name == parent_bone.xml_link_name:
            origin_xml_node = get_origin_from_matrix(this_relative_matrix)
            p_joint.append(origin_xml_node)
        else:
            p_joint.append(ZERO_ORIGIN_NODE) 
        
        joints.append(p_joint)

        parent_link_name = p_xml_link_name
        ret_link = p_xml_link

    # ------------ Create Y joint-------------

    if limits['y']:
        y_joint = ET.Element('joint')
        y_joint.set("name",y_joint_name)
        y_joint.set("type", JOINT_TYPE)

        # create parent and child nodes
        parent_xml_node = ET.Element('parent')
        parent_xml_node.set("link", parent_link_name)

        y_xml_link = ET.Element('link')
        y_xml_link_name = this_bone.xml_link_name + '_Y'
        y_xml_link.set("name", y_xml_link_name)
        links.append(y_xml_link)

        child_xml_node = ET.Element('child')
        child_xml_node.set("link", y_xml_link_name)
        
        y_joint.append(parent_xml_node)
        y_joint.append(child_xml_node)
        y_joint.append(ET.fromstring('<axis xyz="0 0 1"/>'))

        limit_node = ET.fromstring(BASE_LIMIT_NODE_STR)
        limit_node.set('lower', str(limits['y'][0]))
        limit_node.set('upper', str(limits['y'][1]))
        # print(ET.tostring(limit_node, encoding="unicode"))
        y_joint.append(limit_node)

        if parent_link_name == parent_bone.xml_link_name:
            origin_xml_node = get_origin_from_matrix(this_relative_matrix)
            y_joint.append(origin_xml_node)
        else:
            y_joint.append(ZERO_ORIGIN_NODE)

        joints.append(y_joint)

        parent_link_name = y_xml_link_name
        ret_link = y_xml_link

    this_bone.xml_link_name = ret_link.get("name") # this will be used by the next bone to set the correct parent link
    return ret_link

# ==========================================

def export(filename, settings):
    global LINK_NAME_FORMAT, JOINT_NAME_FORMAT, ob, root_bone, links, joints
    counter = 0
    links = []
    joints = []

    if 'armature' in settings:
        ob = settings['armature']
    else:
        ob = bpy.data.objects['Armature']

    # find the root bone, there can be only one
    root_bone = None
    for b in ob.data.bones:
        if not b.parent:
            if root_bone:
                raise Armature_Structure_Error("More than one root bone found")
            root_bone = b    

    if 'link_name_format' in settings:
        LINK_NAME_FORMAT = settings['link_name_format']

    if 'joint_name_format' in settings:
        JOINT_NAME_FORMAT = settings['joint_name_format']

    effort, velocity = (100, 3)

    if 'def_limit_effort' in settings:
        effort = settings['def_limit_effort']

    if 'def_limit_vel' in settings:
        velocity = settings['def_limit_vel']

    set_base_limit_str(effort, velocity)

    create_base_link()
    walk_armature(root_bone, bone_to_urdf)

    #add all the joints and links to the root
    root_xml = ET.Element('robot') # create <robot name="test_robot">
    root_xml.set("name", ob.name)

    root_xml.append(ET.Comment("LINKS"))
    for l in links:
        root_xml.append(l)
        
    root_xml.append(ET.Comment("JOINTS"))
    for j in joints:
        root_xml.append(j)

    #dump the xml string
    ET_raw_string = ET.tostring(root_xml, encoding="unicode")
    dom = minidom.parseString(ET_raw_string)
    ET_pretty_string = dom.toprettyxml()

    with open(filename, "w") as f:
        f.write(ET_pretty_string)

    return ET_pretty_string


if __name__ == '__main__':
        
    urdf_str = export("blender_armature.urdf", {})
    print("\n ======== URDF CREATED ========\n")
    print(urdf_str)
    print("\n ======== URDF saved to blender_armature.urdf ========\n")
    
