bl_info = {
	"name": "Armature to URDF exporter",
	"author": "Bruno Laurencich",
	"version": (0, 0, 0),  #!! Warning is overwritten by the script `set_version.sh` which runs in CI
	"blender": (2, 80, 0),
	"category": "Import-Export",
}

if 'bpy' in locals():
	import imp
	imp.reload(armature_to_urdf)
	imp.reload(gui)
else:
	from . import armature_to_urdf
	from . import gui

import bpy
from bpy_extras.io_utils import ExportHelper
from bpy.props import StringProperty, BoolProperty, FloatProperty
from bpy.types import Operator
from bpy import context, data


def write_urdf(context, filepath, settings):
	urdf_str = armature_to_urdf.export(filepath, settings)
	print("\n ======== URDF CREATED ========\n")
	print(urdf_str)
	print("\n ======== URDF saved to {} ========\n".format(filepath))
	return {'FINISHED'}
	

class ExportURDF(Operator, ExportHelper):
	"""Exports an armature to  URDF (Universal Robot Descriptor Format)"""
	bl_idname = gui.urdf_id_name
	bl_label = "Export Armature to URDF"

	# ExportHelper mixin class uses this
	filename_ext = ".urdf"

	filter_glob: StringProperty(
		default="*.urdf",
		options={'HIDDEN'},
		maxlen=255,  # Max internal buffer length, longer would be clamped.
	)

	# -----------  URDF export props  -----------
	
	link_name_format: StringProperty(name="Link name format", 
							default="link_{counter:02d}_{bone_name}")

	joint_name_format: StringProperty(name="Joint name format", 
							default="joint_{counter:02d}_{bone_name}")

	def_limit_effort: FloatProperty(name="Default <limit> Effort", default=100)
	def_limit_vel: FloatProperty(name="Default <limit> Velocity", default=3)

	def execute(self, context):
		counter = 0	
		the_armature = None

		for ob in bpy.data.objects:
			if ob.type == 'ARMATURE':
				counter += 1
				the_armature = ob

		if counter == 0:
			self.report({'ERROR'}, "No Armature found on the scene, cannot export URDF")
			return {'CANCELLED'}

		settings = {"armature": the_armature, 
			"link_name_format": self.link_name_format, "joint_name_format": self.joint_name_format,
			"def_limit_effort": self.def_limit_effort, "def_limit_vel" : self.def_limit_vel  }


		if counter == 1:
			bpy.ops.object.mode_set(mode='OBJECT')
			return write_urdf(context, self.filepath, settings)

		if counter > 1:
			bpy.ops.object.mode_set(mode='OBJECT')
			if not context.object or context.object.type != 'ARMATURE':
				self.report({'ERROR'}, "No active armature found, please select only one armature from the scene ")
				return {'CANCELLED'}

			settings["armature"] = context.object 
			return write_urdf(context, self.filepath, settings)

def menu_func_export(self, context):
	self.layout.operator(ExportURDF.bl_idname, text="Universal Robot Descriptor Format (.urdf)")


def register():
	#TODO: group properties

	bpy.utils.register_class(ExportURDF)
	bpy.types.TOPBAR_MT_file_export.append(menu_func_export)

	bpy.types.Bone.xml_link_name = bpy.props.StringProperty(name="URDF xml link name", 
							default="unset")

	bpy.types.Bone.body_segment_mass = bpy.props.FloatProperty(name="URDF body segment mass", 
								default=5.0)

	bpy.types.Bone.ixx = bpy.props.FloatProperty(name="Inertia value XX", 
								default=1.0)
	bpy.types.Bone.iyy = bpy.props.FloatProperty(name="Inertia value YY", 
								default=1.0)
	bpy.types.Bone.izz = bpy.props.FloatProperty(name="Inertia value ZZ", 
								default=1.0)

	bpy.types.Bone.ixy = bpy.props.FloatProperty(name="Inertia value XY", 
								default=0.0)

	bpy.types.Bone.ixz = bpy.props.FloatProperty(name="Inertia value XZ", 
								default=0.0)

	bpy.types.Bone.iyz = bpy.props.FloatProperty(name="Inertia value YZ", 
								default=0.0)
	
	bpy.types.EditBone.body_segment_mass = bpy.props.FloatProperty(name="URDF body segment mass", 
								default=5.0 )

	bpy.types.EditBone.ixx = bpy.props.FloatProperty(name="Inertia value XX", 
								default=1.0)
	bpy.types.EditBone.iyy = bpy.props.FloatProperty(name="Inertia value YY", 
								default=1.0)
	bpy.types.EditBone.izz = bpy.props.FloatProperty(name="Inertia value ZZ", 
								default=1.0)

	bpy.types.EditBone.ixy = bpy.props.FloatProperty(name="Inertia value XY", 
								default=0.0)

	bpy.types.EditBone.ixz = bpy.props.FloatProperty(name="Inertia value XZ", 
								default=0.0)

	bpy.types.EditBone.iyz = bpy.props.FloatProperty(name="Inertia value YZ", 
								default=0.0)


	gui.register()


def unregister(): # pragma: no cover
	bpy.utils.unregister_class(ExportURDF)
	bpy.types.TOPBAR_MT_file_export.remove(menu_func_export)

	gui.unregister()