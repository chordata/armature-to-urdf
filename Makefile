ifeq ($(BLENDER_BIN),)
BLENDER_BIN := $(shell which blender)
endif

ifeq ($(BLEND_FILE),)
BLEND_FILE := avatar.blend
endif


all:
ifndef BLENDER_BIN
	$(error The blender binary was not found. Make sure it's in your path or set the env variable BLENDER_BIN to its location )
endif
	$(BLENDER_BIN) -b $(BLEND_FILE) --python ./addons/armature-to-urdf/armature_to_urdf.py 