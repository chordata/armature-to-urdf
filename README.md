# Blender armature to URDF converter

This simple addon parses a Blender armature Object and builds an XML URDF description

![urf_avatar](urf_avatar.jpg)


## Installation

Download the lastest automatically generated .zip here:

[![urdf_addon_download_badge](https://img.shields.io/badge/Download%20zip%20-Armature%20to%20URDF%20addon-orange)](https://gitlab.com/chordata/armature-to-urdf/-/jobs/artifacts/master/browse?job=zip)

Install the .zip on the Blender preferences dialog.

## Usage (Blender GUI)

Select an armature and go to `File > Export > Universal Robot Descriptor Format (URDF)`

## Usage (Bash)

Just execute `make`, it will look for a Blender binary in your system and use it to create a file called `blender_armature.urdf`. This requires the file to have only one armature. 


